package lab5;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class SphereTest {
    
    @Test
    public void test_sphere_constructor_throwsIllegalArgumentException(){
        try {
            Sphere sphere = new Sphere(-5);
            fail("sphere should throw an error if the radius is less than or equal to 0");
        } catch (IllegalArgumentException IAE) {
            
        }
        
    }
    
    @Test
    public void test_sphere_getRadius_returns5(){
        Sphere sphere = new Sphere(5);
        assertEquals("testing sphere getRadius returns 5",5.0,sphere.getRadius(),0.5);
    }

 
    @Test
    public void sphere_getVolume_returns25dot13(){
        Sphere sphere = new Sphere(2);
        assertEquals("testing getVolume returns 25.13", 25.13, sphere.getVolume(), 0.5);
    }

    @Test
    public void sphere_getSurfaceArea_returns50dot26(){
        Sphere sphere = new Sphere(2);
        assertEquals("testing getSurfaceArea returns 50.26", 50.26, sphere.getSurfaceArea(), 0.5);
    }
}
