package lab5;
import static org.junit.Assert.*;
import org.junit.Test;

public class CylinderTests{

    @Test
    public void testConstructorForRadius(){
        try{
            Cylinder cyl = new Cylinder(-25, 5);
            fail("Should fail since radius is negative");
        } catch(IllegalArgumentException e){

        }
    }

    @Test
    public void testConstructorForHeight(){
        try{
            Cylinder cyl = new Cylinder(5, -10);
            fail("Should fail since height is negative");
        } catch(IllegalArgumentException e){

        }
    }

    @Test
    public void testConstructor(){
        Cylinder cyl = new Cylinder(5, 10);
    }

    @Test
    public void testGetRadius(){
        Cylinder cyl = new Cylinder(5, 10);
        assertEquals("test for getRadius(should return 5.0)", 5.0, cyl.getRadius(), 0.5);
    }

    @Test
    public void testGetHeight(){
        Cylinder cyl = new Cylinder(3, 7);
        assertEquals("test for getHeight(should return 7.0)", 7.0, cyl.getHeight(), 0.5);
    }

    @Test
    public void testGetVolume(){
        Cylinder cyl = new Cylinder(5, 15); 
        assertEquals("test the getVolume method(should return 1178.09)", 1178.09, cyl.getVolume(), 0.5);
    }

    @Test
    public void testGetSurfaceArea(){
        Cylinder cyl = new Cylinder(2, 12);
        assertEquals("test the getSurfaceArea(should return 175.93)", 175.93, cyl.getSurfaceArea(), 0.5);
    }
}