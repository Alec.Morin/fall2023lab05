/** 
 * @author Alec Morin
 * @version 10/3/2023
 */

 package lab5;

 public class Sphere implements IShape3d{

    private double radius;

    public Sphere(double radius){
        if(radius <= 0){
            throw new IllegalArgumentException("oh no this doesnt work!");
        }
        this.radius = radius;
    }

    /**
     * @returns the radius of the object
     */
    public double getRadius(){
        return this.radius;
    }

    /** 
     * @returns the volume
    */
    @Override
    public double getVolume() {
        return (4/3*Math.PI*Math.pow(this.radius, 3));
    }

    /**
     * @returns the surface area
     */
    @Override
    public double getSurfaceArea() {
        return (4*Math.PI * Math.pow(this.radius, 2));
    }

 }