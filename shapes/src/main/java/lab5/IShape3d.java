package lab5;

public interface IShape3d{
    double getVolume();
    double getSurfaceArea();
}