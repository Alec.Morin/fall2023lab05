/**
 * Cylinder is a class used to define a shape using the Shape3D interface
 * @author Adam Winter
 * @version 10/3/2023
 */

package lab5;

public class Cylinder implements IShape3d {

    private double radius;
    private double height;
    
    public Cylinder(double radius, double height) {
        if(radius <=0 || height <= 0){
            throw new IllegalArgumentException("Radius and height cannot be negative");
        }
        this.radius = radius;
        this.height = height;
    }

    /**
     * returns the value for this object's radius
     * @return value of radius
     */
    public double getRadius() {
        return this.radius;
    }
    /**
     * returns the value for this object's height
     * @return value of height
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * return the value of the cylinder's volume
     * @return value of volume
     * 
     */
    @Override
    public double getVolume() {
        return Math.PI * (this.radius * this.radius * this.height);
    }

    /**
     * return the value of the cylinder's surface area
     * @return value of surfaceArea
     */
    @Override
    public double getSurfaceArea() {
        return (2 * Math.PI) * (this.radius * this.radius) + ( (2 * Math.PI) * this.radius * this.height);
    }
    
}
